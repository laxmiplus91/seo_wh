<?php



include_once("config/config.php");
include_once("functions.php");

?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<title>
		SEO-Wharehouse
	</title>


	<meta name="description" content="">

	<meta name="viewport" content="width=device-width">	

	
	<!--Bootstrap CSS file-->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<!-- <link rel="stylesheet" href="css/jquery.datetimepicker.css"> -->
	<link rel="stylesheet" href="css/wbbtheme.css">
	<link rel="stylesheet" href="css/bootstrap-timepicker.min.css">
	
	<link rel="stylesheet" href="css/jquery-ui.css">
	<link href="css/jquery.dataTables.css" rel="stylesheet">
	<link rel="stylesheet" href="css/font-awesome.min.css">

	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/jquery.timepicker.css">
	
	<script src="js/jquery-1.10.2.js"></script>

  	<!--Bootstrap JS file-->

	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	
	<!--Custom Validation JS-->
	<script type="text/javascript" src="js/validation.js"></script>
  	
  	<script src="js/jquery-ui.js"></script>
  	<script src="js/jquery.dataTables.min.js"></script>

	<script type="text/javascript" src="js/jquery.timepicker.min.js"></script>
  	<!--<script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>   -->
  	
  	<!--
	<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.5.3/modernizr.min.js"></script>
	<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.0/jquery.js"></script>
	-->
	<!-- Latest compiled and minified JavaScript -->
	


	<script type="text/javascript">
		var default_search_text = "CLICK TO SEARCH";
		
	$(window).load(function() {
		
		
		nav();
	});

	function nav()
	{
		
	}

	function displayAlert(notes)
	{
		alert("The feature to manage "+notes+" is disabled");
	}
	</script>
	
</head>
<body>
<div class="classPageBoxModel" id="id_page">