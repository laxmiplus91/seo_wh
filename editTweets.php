<?php

require_once "config/config.php";
// Include for <head></head tag.
include ('appHeaderBase.php');
// Include for body header.
include ('appHeaderPage.php');
?>

<script type="text/javascript">

  $(document).ready (function(){
    $("#idHideOnClick").click(function(){
      $("#idHideOnClick").hide();                  
    });
  });

</script>

<script type="text/javascript">

  // Date Picker Call Code Start
  $(function() {
    $( "#idTweetDate" ).datepicker({
      dateFormat: "mm-dd-yy",
      changeMonth: true,
      minDate: 0

    });
  });  

  $(function() {
    $('#idTweetTime').timepicker({      
     'timeFormat': 'h:i:s A'     
    });
  });

</script>


<?php

$aAllTweets= array();
$aAllTweets= getAllTweets();


// Display an alert Message.
if(isset($_GET['alerts'])){   
    $sAlert=$_GET['alerts'];
    echo '<div class="alert alert-info" id="idHideOnClick">';
    echo constant(displayAlert($sAlert));
    echo '</div>';   
}


?>

<div class="container">
      <!-- Example row of columns -->

      <div class="row">
        <div class="col-md-9">
          <p><h2>Welcome </h2></p>
          <p><h4 class="classOrangeColor">Manage Tweets</h4>          
        </div>
      </div>

      <div class="row">
        <div class="col-md-5">
            <p><h2>Update </h2></p>
            <form name="AddEmployee" method="POST" action="addTweetEmulator.php" class="form-horizontal" role="form">
                <fieldset>
                  <legend>
                    Tweet + Link = <span class="" id="idTextCounter">140 Char</span>
                  </legend>

                  <div class="form-group">
                    <label class="col-md-3 control-label">Tweet Text</label>
                    <div class="col-md-9">
                      <textarea class="form-control classCustomTextArea" id="idTweetTxt" placeholder="Write your tweet" rows="5" name="tweetTxt" required></textarea>                        
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-md-3 control-label">Tweet Link</label>
                    <div class="col-md-9">
                      <input type="text" class="form-control" id="idTweetLink" placeholder="www.yourlink.com" name="tweetLink" >
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Tweet Schedule Time</label>
                    <div class="col-md-9">
                      <div class="row">
                        <div class="col-md-6">
                          <input type="text" class="form-control" id="idTweetDate" placeholder="dd/mm/yy" name="tweetDate" required>
                        </div>
                        <div class="col-md-6">
                          <input type="text" class="form-control" id="idTweetTime" placeholder="h:m:s" name="tweetTime" required>
                        </div>
                      </div>
                      
                    </div>
                  </div>


                  <div class="form-group">  
                    <label class="col-md-3 control-label">Tweet Category</label>
                    <div class="col-md-9">
                      <select name="tweetCat" id="idTweetCat" class="form-control classBottom10BMargin"/>
                        <option value="1">Default</option>
                      </select>
                    </div>
                    <p class="help-block" id="idEmpBossIdMsg"></p><hr/>  
                  </div>
                
                  <div class="form-group">
                    <label class="col-md-3 control-label"></label>
                      <div class="col-md-9">
                        <button class="btn btn-primary classBottom10BMargin" name="addTweet" id="idAddTweet" type="submit">Add</button>
                    </div>
                  </div>                 
                </fieldset>
            </form>
        </div>
        <div class="col-md-7">
          <div class="row">
              <div class="col-md-12">
                <h2>All Tweets <span class="pull-right"><a href="generate_tweets_csv.php">Export Tweets</a></span></h2>
              </div>
          </div>
           <div class="row classCustomRowBottom">
            <div class="col-md-1 classOrangeColor">#</div>
            <div class="col-md-8 classOrangeColor">Tweet</div>
            <div class="col-md-3 classOrangeColor">Tweet Schedule at</div>
          </div> 
          <?php
          $ii=1;          
            foreach ($aAllTweets as $value) {
          ?>
            <div class="row classCustomRow">
              <div class="col-md-1 classCustomIcon class30Lineheight"><?php echo $ii; ?></div>
              <div class="col-md-8 classCustomTweetTxt class30Lineheight"><?php echo $value['tweet_txt'].'<a href="http://'.$value['tweet_link'].'" target="_blank">"'.$value['tweet_link']; ?> </a></div>
              <div class="col-md-3 classCustomDAT class30Lineheight"><?php echo $value['tweet_date_and_time']; ?></div>
            </div>  
          <?php
            $ii++;
            }
          ?>
            
        </div>
      </div>
    </div>
  
<?php
// Include for body Footer.
include ('appFooterPage.php');
include ('appFooterBase.php');

?>