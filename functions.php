<?php
/*
Including Database Manager class
*/
include ('classes/class.DBConnManager.php');
include_once("config/config.php");

/*
Userdefine Funtion Library of SEO_Wharehouse.
*/



function addTweets($sTweetTxt, $sTweetLink, $iTweetCat, $sTweetDAT){

        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();       	
       	$sAddedOn= date('Y-m-d');
       	$iDeleteStatus = 0;
       	$sTableName=DATABASE_TABLE_PREFIX."tweets";
       	// Query to insert the tweet in the database.
        $sQuery = "INSERT INTO `{$sTableName}` (`tweet_id`, `tweet_txt`, `tweet_link`, `tweet_date_and_time`, `cat_id`, `tweet_doi`, `tweet_del_status`) 
            VALUES (NULL, '{$sTweetTxt}', '{$sTweetLink}', '{$sTweetDAT}', '{$iTweetCat}', '{$sAddedOn}', '{$iDeleteStatus}');";
        $conn1 = $DBMan->getConnInstance(); 
        $bResult = $conn1->query($sQuery);
        if(!$bResult){
            return FALSE;
        }        

	if($bResult==TRUE){
		$idValue= TRUE;	
	}else{
		$idValue=FALSE;
	}
	
	return $idValue;
}

function updateTweets($iTweetId, $sTweetTxt, $sTweetLink, $sTweetCat, $sTweetDAT){

  $DBMan = new DBConnManager();
  $conn =  $DBMan->getConnInstance();         
  $sAddedOn= date('Y-m-d');
  $iDeleteStatus = 0;
  $sTableName=DATABASE_TABLE_PREFIX."tweets";
  
  $sQuery = "UPDATE `{$sTableName}` SET `tweet_txt`='{$sTweetTxt}', `tweet_link`='{$sTweetLink}', `tweet_date_and_time`='{$sTweetDAT}' WHERE `tweet_id`= '{$iTweetId}'"; 
  var_dump($sQuery);
  $sResult = $conn->query($sQuery);    
  if(!$sResult){      
      return FALSE;
  }else{
      return TRUE;
  }

}


function getAllTweets(){
  $DBMan = new DBConnManager();
  $conn =  $DBMan->getConnInstance();         
  $sAddedOn= date('Y-m-d');
  $iDeleteStatus = 0;
  $sTableName=DATABASE_TABLE_PREFIX."tweets";
  // Query to retrive all the tweet in the database.
  $sQuery = "SELECT * FROM {$sTableName} WHERE tweet_del_status=0 ORDER BY tweet_id DESC";
  $rResult = $conn->query($sQuery);
  $aAllTweet = array();
  if($rResult){
     while ($aRow = $rResult->fetch_array()) {
          $aAllTweet[] = $aRow;
      }
  }

  return $aAllTweet;
}

function getOneTweets($iTweetId){
  $DBMan = new DBConnManager();
  $conn =  $DBMan->getConnInstance();         
  $sAddedOn= date('Y-m-d');
  $iDeleteStatus = 0;
  $sTableName=DATABASE_TABLE_PREFIX."tweets";
  // Query to retrive tweet by ID.
  $sQuery = "SELECT * FROM {$sTableName} WHERE tweet_id={$iTweetId} and tweet_del_status=0 ORDER BY tweet_id DESC";
  $rResult = $conn->query($sQuery);
  $aTweetDetail = array();
  if($rResult){
     while ($aRow = $rResult->fetch_array()) {
          $aTweetDetail = $aRow;
      }
  }

  return $aTweetDetail;
}

//! rediects user to specified url with specified error/warning/success messages.
//! @param $url string URL to which user has to be redirected.
//! @param $msgCodes array array of messages.
function redirectWithAlert($url, $msgCodes){
  $msgCodes = (array)$msgCodes;
  $sMsg = implode("::x::",$msgCodes);
  $sCode = base64_encode($sMsg);

  if(strstr($url,'?')!==FALSE){
    $fURL =  $url."&alerts={$sCode}";
  }
  else {
    $fURL =  $url."?alerts={$sCode}";
  }
  
  header("Location: {$fURL}");
  
}

/*
Function to display the appropriate aler message
*/
function displayAlert($sAlert){
  return  base64_decode($sAlert);
}

function generateCSVFile(){

  $DBMan = new DBConnManager();
  $conn =  $DBMan->getConnInstance();         
  $iDeleteStatus = 0;
  $sTableName=DATABASE_TABLE_PREFIX."tweets";
  
   // Query to retrive the tweet in the database.
  $sQuery = "SELECT `tweet_date_and_time`, `tweet_txt`, `tweet_link` FROM {$sTableName} WHERE tweet_del_status=0 ORDER BY tweet_id ASC";
  
  $rResult = $conn->query($sQuery);
  
  $aAllTweet = array();
  
  // output headers so that the file is downloaded rather than displayed
  header('Content-Type: text/csv; charset=utf-8');  
  header('Content-Disposition: attachment; filename=data.csv');

  // create a file pointer connected to the output stream
  $output = fopen('php://output', 'w'); 
  
  
  if($rResult){
     while ($aRow = $rResult->fetch_array()) {          
        fputcsv($output, array($aRow['tweet_date_and_time'], $aRow['tweet_txt'], $aRow['tweet_link']));        
      }   
  }
 exit();
}

?>