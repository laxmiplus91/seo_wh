<?php

require_once "config/config.php";
// Include for <head></head tag.
include ('appHeaderBase.php');
// Include for body header.
include ('appHeaderPage.php');
?>

<script type="text/javascript">

  $(document).ready (function(){
    $("#idHideOnClick").click(function(){
      $("#idHideOnClick").hide();                  
    });
  });

</script>

<script type="text/javascript">

  // Date Picker Call Code Start
  // $(function() {
  //   $( "#idTweetDate" ).datepicker({
  //     dateFormat: "mm-dd-yy",
  //     changeMonth: true,
  //     defaultDate: "+1w",
  //     minDate: 0

  //   });
  // });  

  // $(function() {
  //   $('#idTweetTime').timepicker({      
  //    'timeFormat': 'h:i:s A'     
  //   });
  // });

</script>


<?php

$aAllTweets= array();
$aAllTweets= getAllTweets();



?>

<div class="container">
      <!-- Example row of columns -->

      <div class="row">
        <div class="col-md-9">
          <p><h2>Welcome </h2></p>
          <p><h4 class="classOrangeColor">Tweets List</h4>          
        </div>
      </div>

      <div class="row">        
        <div class="col-md-12">
          <div class="row">
              <div class="col-md-12">
                <h2>All Tweets <span class="pull-right"><a href="generate_tweets_csv.php">Export Tweets</a></span></h2>
              </div>
          </div>
          <div class="row classCustomRowBottom">
            <div class="col-md-1 classOrangeColor">#</div>
            <div class="col-md-8 classOrangeColor">Tweet</div>
            <div class="col-md-3 classOrangeColor">Tweet Schedule at</div>
          </div> 
          <?php
          $ii=1;          
            foreach ($aAllTweets as $value) {
          ?>
            <div class="row classCustomRow">
              <div class="col-md-1 classCustomIcon class30Lineheight"><?php echo $ii."<br/><a style='text-decoration:none;color:#fff;' href=editTweet.php?id=".$value['tweet_id'].">Edit</a>"; ?></div>
              <div class="col-md-8 classCustomTweetTxt class30Lineheight"><?php echo $value['tweet_txt'].'<a href="http://'.$value['tweet_link'].'" target="_blank">'.$value['tweet_link']; ?> </a></div>
              <div class="col-md-3 classCustomDAT class30Lineheight"><?php echo $value['tweet_date_and_time']; ?></div>
            </div>    
          <?php
            $ii++;
            }
          ?>
            
        </div>
      </div>
    </div>
  
<?php
// Include for body Footer.
include ('appFooterPage.php');
include ('appFooterBase.php');

?>