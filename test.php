 
<script type="text/javascript">

 $( "#fromDate" ).datepicker({
dateFormat: "yy-mm-dd",
defaultDate: "+1w",
changeMonth: true,
numberOfMonths: 2,
maxDate: 0,
onClose: function( selectedDate ) {
$( "#to" ).datepicker( "option", "minDate", selectedDate );
}
});
 
 
          $('#idArrivalTime').timepicker({
            hourGrid: 4,
            minuteGrid: 10,
            showTimezone: true,
            timeFormat: 'HH:mm'        
        });

 </script>