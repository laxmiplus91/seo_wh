<?php

//! @file 
/*! This file contains all alert message constants. 
*	Constants starting with S prefix are the Success Messages
*	Constants starting with W prefix are the Warning Messages
*	Constants starting with E prefix are the Error Messages
*
*/

//! Sucess Messages
define('S1', "Tweets Added successfully.");
define('S2', "Tweets Updated successfully.");

//! Error Messages
define('E1', "Try Again!, Tweet not added.");
define('E2', "Try Again!, Tweet not updated.");

//! DB Error Messages
define('D1', "Technical Error");

//! Warning Messages


?>