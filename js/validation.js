$(document).ready(function(){

	/*Add Employee Form Validation*/
	$('#idAddEmployeeSubmit').click(function(){
		
		if($('#idEmpDesignationId').val() == ''){
			 $('#idEmpDesignationIdMsg').html("<span class='classRedFont'>Select Designation</span>");

			$('#idEmpDesignationId').focus();
			return false;
		}else{
			$('#idEmpDesignationIdMsg').html("");			
		}

		if($('#idUserType').val() == ''){
			 $('#idUserTypeMsg').html("<span class='classRedFont'>Please Select User Type</span>");

			$('#idUserType').focus();
			return false;
		}else{
			$('#idUserTypeMsg').html("");			
		}

		if($('#idEmpBossId').val() == ''){
			 $('#idEmpBossIdMsg').html("<span class='classRedFont'>Please Allot Boss</span>");

			$('#idEmpBossId').focus();
			return false;
		}else{
			$('#idEmpBossIdMsg').html("");			
		}

	});

	/*Add Designation Form Validation*/
	$('#idAddDesignationSubmit').click(function(){
		
		if($('#idDesignationPriority').val() == ''){
			 $('#idDesignationPriorityMsg').html("<span class='classRedFont'>Select Designation Priority</span>");

			$('#idDesignationPriority').focus();
			return false;
		}else{
			$('#idDesignationPriorityMsg').html("");			
		}
	});


	/*Add Leave Request Form Validation*/
	$('#idRequestLeaveFormSubmit').click(function(){

		if($('#idLeaveExpDuration').val() == ''){
			 $('#idLeaveExpDurationMsg').html("<span class='classRedFont'>Select Leave Expiry Duration</span>");

			$('#idLeaveExpDuration').focus();
			return false;
		}else{
			$('#idLeaveExpDurationMsg').html("");			
		}

		if($('#idLeaveCarryForwardDuration').val() != ''){
			 if (validateNumberOnly('idLeaveCarryForwardDuration')){
			 	$('#idLeaveCarryForwardDurationMsg').html("<span class='classRedFont'>Numberic Value Only</span>");
				$('#idLeaveCarryForwardDuration').focus();
				return false;
			 }else{
			 	return true;
			 }
			
		}else{
			$('#idLeaveCarryForwardDurationMsg').html("");			
		}
		

		if($('#idDesignationId').val() == ''){
			 $('#idDesignationIdMsg').html("<span class='classRedFont'>Select Leave Expiry Duration</span>");

			$('#idDesignationId').focus();
			return false;
		}else{
			$('#idDesignationIdMsg').html("");			
		}

		if($('#idLeaveTypeId').val() == ''){
			 $('#idLeaveTypeIdMsg').html("<span class='classRedFont'>Select Leave Type</span>");

			$('#idLeaveTypeId').focus();
			return false;
		}else{
			$('#idLeaveTypeIdMsg').html("");			
		}
	});
	
	

});


function validateTextOnly(txtOnlyId){
	var textOnly = document.getElementById(txtOnlyId).value;
    var filter = /^[a-zA-Z\s]+$/;
    if (filter.test(textOnly)) {
        return true;
    }
    else {
        return false;
    }
}

function validateNumberOnly(noOnlyId) {
    var iNumber = document.getElementById(noOnlyId).value;
    var filter = /^[0-9-+]+$/;
    if (filter.test(iNumber)) {
        return true;
    }
    else {
        return false;
    }
}

function validateEmail(txtEmail) {
    var email = document.getElementById(txtEmail).value;
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(email)) {
        return true;
    }
    else {
        return false;
    }
}
