-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 26, 2014 at 03:59 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `seo_wh`
--

-- --------------------------------------------------------

--
-- Table structure for table `wh_cat`
--

CREATE TABLE IF NOT EXISTS `wh_cat` (
  `cat_id` int(10) NOT NULL COMMENT 'Primary Key',
  `cat_name` varchar(1000) NOT NULL COMMENT 'Category name',
  `cat_doi` date NOT NULL COMMENT 'Date of adding category',
  `cat_del_status` int(10) NOT NULL COMMENT '1 for deleted record',
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wh_links`
--

CREATE TABLE IF NOT EXISTS `wh_links` (
  `link_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `link_txt` varchar(1000) NOT NULL COMMENT 'use to store the exact link',
  `link_doi` date NOT NULL COMMENT 'Date of record insertion',
  `link_del_status` int(10) NOT NULL COMMENT '1 for deleted record',
  PRIMARY KEY (`link_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `wh_tweets`
--

CREATE TABLE IF NOT EXISTS `wh_tweets` (
  `tweet_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'primary Key of wh_tweets',
  `tweet_txt` varchar(140) NOT NULL COMMENT 'Tweets text',
  `tweet_link` varchar(1000) NOT NULL COMMENT 'Use to store the tweet link',
  `tweet_date_and_time` varchar(100) NOT NULL COMMENT 'Tweet schedule Date and time ',
  `cat_id` int(10) NOT NULL COMMENT 'Use to store the tweet Category',
  `tweet_doi` date NOT NULL COMMENT 'Date of record inserted',
  `tweet_del_status` int(11) NOT NULL COMMENT '1 for deleted record',
  PRIMARY KEY (`tweet_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
